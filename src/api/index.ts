import { Api } from "@/types"
import { httpClient } from "@/api/httpClient"

export const api: Api = {
    outages: async () => {
        return await httpClient.get("outages")
    },

    site: async (siteName) => {
        return await httpClient.get(`site-info/${siteName}`)
    },

    siteOutages: async (siteName, siteOutages) => {
        return await httpClient.post(`site-outages/${siteName}`, siteOutages)
    },
}
