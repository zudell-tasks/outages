export interface Device {
    id: string
    name: string
}

export interface Site {
    id: string
    name: string
    devices: Device[]
}

export interface SiteDataSource {
    get: (siteName: string) => Promise<Site>
}
