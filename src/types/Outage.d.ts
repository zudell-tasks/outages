export interface Outage {
    id: string
    begin: string
    end: string
}

export interface OutageDataSource {
    get: () => Promise<Outage[]>
}
