import { Outage } from "./Outage"
import { Site } from "./Site"
import { SiteOutage } from "./SiteOutage"
import { AxiosResponse } from "axios"

export interface Api {
    outages: () => Promise<AxiosResponse<Outage[]>>
    site: (siteName: string) => Promise<AxiosResponse<Site>>
    siteOutages: (
        siteName: string,
        siteOutages: SiteOutage[],
    ) => Promise<undefined>
}
