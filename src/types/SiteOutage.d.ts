export interface SiteOutage {
    id: string
    name: string
    begin: string
    end: string
}

export interface SiteOutageDataSource {
    save: (siteName: string, siteOutages: SiteOutage[]) => Promise<void>
}
