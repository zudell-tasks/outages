import { SiteDataSource } from "../../types"

export const mockSiteService: SiteDataSource = {
    async get() {
        return {
            id: "1",
            name: "test site 1",
            devices: [
                {
                    id: "1",
                    name: "test device 1",
                },
                {
                    id: "2",
                    name: "test device 2",
                },
                {
                    id: "3",
                    name: "test device 3",
                },
                {
                    id: "4",
                    name: "test device 4",
                },
            ],
        }
    },
}

export const mockSiteServiceWithoutMatchingDeviceNames: SiteDataSource = {
    async get() {
        return {
            id: "1",
            name: "test site 1",
            devices: [
                {
                    id: "100_001",
                    name: "test device 100,001",
                },
                {
                    id: "100_002",
                    name: "test device 100,002",
                },
            ],
        }
    },
}
