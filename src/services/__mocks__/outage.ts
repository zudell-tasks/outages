import { OutageDataSource } from "../../types"

export const mockOutageService: OutageDataSource = {
    async get() {
        return [
            {
                id: "1",
                begin: new Date(Date.UTC(2023, 1, 1)).toISOString(),
                end: new Date(Date.UTC(2023, 2, 2)).toISOString(),
            },
            {
                id: "2",
                begin: new Date(Date.UTC(2023, 2, 1)).toISOString(),
                end: new Date(Date.UTC(2023, 3, 2)).toISOString(),
            },
        ]
    },
}

export const mockOutageServiceWithOldData: OutageDataSource = {
    async get() {
        return [
            {
                id: "3",
                begin: new Date(Date.UTC(2021, 1, 1)).toISOString(),
                end: new Date(Date.UTC(2021, 2, 2)).toISOString(),
            },
            {
                id: "4",
                begin: new Date(
                    Date.UTC(2021, 11, 31, 23, 59, 59, 999),
                ).toISOString(),
                end: new Date(Date.UTC(2021, 3, 2)).toISOString(),
            },
        ]
    },
}
