import { SiteOutageDataSource } from "../../types"

export const mockSiteOutageDataService: SiteOutageDataSource = {
    async save() {
        return undefined
    },
}
