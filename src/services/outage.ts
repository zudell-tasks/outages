import { OutageDataSource } from "@/types"
import { api } from "@/api"

export const outageDataSource: OutageDataSource = {
    async get() {
        const response = await api.outages()

        return response.data
    },
}
