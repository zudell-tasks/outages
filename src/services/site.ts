import { SiteDataSource } from "@/types"
import { api } from "@/api"

export const siteDataSource: SiteDataSource = {
    async get(siteName) {
        const response = await api.site(siteName)

        return response.data
    },
}
