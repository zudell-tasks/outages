import { SiteOutageDataSource } from "@/types"
import { api } from "@/api"

export const siteOutageDataSource: SiteOutageDataSource = {
    async save(siteName, siteOutages) {
        return await api.siteOutages(siteName, siteOutages)
    },
}
