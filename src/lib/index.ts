import {
    outageDataSource,
    siteDataSource,
    siteOutageDataSource,
} from "@/services"
import { Outage, SiteOutage } from "@/types"

const defaultProcessCount = parseInt(process.env.PROCESS_COUNT || "100")
const defaultSitename = process.env.SITE_NAME || "norwich-pear-tree"
const defaultThreshold = process.env.THRESHOLD_DATE || "2022-01-01T00:00:00Z"

export const combineOutageWithDeviceName = async ({
    outageService = outageDataSource,
    processCount = defaultProcessCount,
    siteName = defaultSitename,
    siteOutageService = siteOutageDataSource,
    siteService = siteDataSource,
    threshold = defaultThreshold,
}) => {
    const siteData = await siteService.get(siteName)
    const siteDeviceIdMap = siteData.devices.reduce<Record<string, string>>(
        (accumulator, device) => {
            accumulator[device.id] = device.name

            return accumulator
        },
        {},
    )

    const outageList = await outageService.get()
    const outagesAfterThreshold = filterOldOutages(outageList, threshold)

    for (const outageChunk of arrayChunks(
        outagesAfterThreshold,
        processCount,
    )) {
        const siteOutages: SiteOutage[] = []

        for (const outage of outageChunk) {
            const name = siteDeviceIdMap[outage.id]

            if (!name) {
                continue
            }

            siteOutages.push({
                ...outage,
                name,
            })
        }

        if (siteOutages.length > 0) {
            await siteOutageService.save(siteName, siteOutages)
        }
    }
}

const filterOldOutages = (outages: Outage[], threshold: string) => {
    const thresholdDate = new Date(threshold)

    return outages.filter((outage) => {
        const date = new Date(Date.parse(outage.begin))

        return date >= thresholdDate
    })
}

function* arrayChunks<T>(array: T[], chunkSize: number) {
    for (let i = 0; i < array.length; i += chunkSize) {
        yield array.slice(i, i + chunkSize)
    }
}
