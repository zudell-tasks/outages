import { combineOutageWithDeviceName } from "./"

import {
    mockOutageService,
    mockOutageServiceWithOldData,
    mockSiteOutageDataService,
    mockSiteService,
    mockSiteServiceWithoutMatchingDeviceNames,
} from "../services/__mocks__"

describe("combinedOutageWithDeviceName", () => {
    describe("with expected data", () => {
        it("gets data from the outages service", async () => {
            const outageServiceSpy = jest.spyOn(mockOutageService, "get")
            await combineOutageWithDeviceName({
                outageService: mockOutageService,
                siteOutageService: mockSiteOutageDataService,
                siteService: mockSiteService,
            })
            expect(outageServiceSpy).toHaveBeenCalledTimes(1)
        })

        it("gets data from the sites service", async () => {
            const siteServiceSpy = jest.spyOn(mockSiteService, "get")
            await combineOutageWithDeviceName({
                outageService: mockOutageService,
                siteOutageService: mockSiteOutageDataService,
                siteService: mockSiteService,
            })
            expect(siteServiceSpy).toHaveBeenCalledTimes(1)
        })

        it("sends data to the site outages service", async () => {
            const expectedSiteName = "test-site"
            const expectedOutages = [
                {
                    id: "1",
                    begin: "2023-02-01T00:00:00.000Z",
                    end: "2023-03-02T00:00:00.000Z",
                    name: "test device 1",
                },
                {
                    id: "2",
                    begin: "2023-03-01T00:00:00.000Z",
                    end: "2023-04-02T00:00:00.000Z",
                    name: "test device 2",
                },
            ]
            const siteOutageDataServiceSpy = jest.spyOn(
                mockSiteOutageDataService,
                "save",
            )
            await combineOutageWithDeviceName({
                outageService: mockOutageService,
                siteName: expectedSiteName,
                siteOutageService: mockSiteOutageDataService,
                siteService: mockSiteService,
            })
            expect(siteOutageDataServiceSpy).toHaveBeenCalledWith(
                expectedSiteName,
                expectedOutages,
            )
        })
    })

    describe("with old outages", () => {
        it("should not send any data to the site outages service", async () => {
            const siteOutageDataServiceSpy = jest.spyOn(
                mockSiteOutageDataService,
                "save",
            )
            await combineOutageWithDeviceName({
                outageService: mockOutageServiceWithOldData,
                siteOutageService: mockSiteOutageDataService,
                siteService: mockSiteService,
            })
            expect(siteOutageDataServiceSpy).not.toHaveBeenCalled()
        })
    })

    describe("without matching device names", () => {
        it("should not send any data to the site outages service", async () => {
            const siteOutageDataServiceSpy = jest.spyOn(
                mockSiteOutageDataService,
                "save",
            )
            await combineOutageWithDeviceName({
                outageService: mockOutageService,
                siteOutageService: mockSiteOutageDataService,
                siteService: mockSiteServiceWithoutMatchingDeviceNames,
            })
            expect(siteOutageDataServiceSpy).not.toHaveBeenCalled()
        })
    })
})
