## Usage

```bash
npm install
```
to install the necessary dependencies

```bash
npm run build
```

to transpile the typescript to javascript

```bash
node dist/cmd/combineOutageWithDeviceName.js
```

to combine the outage data, with the device data, and send the required data
to the `site-info` endpoint

## Configuration

There are some required and optional environment variables

### required
```bash
API_BASE_URL
API_KEY
```

### optional
```bash
PROCESS_COUNT
SITE_NAME
THRESHOLD_DATE
```

- process count: this size of batches of outages to process and send to the `site-outages` endpoint
- site name: the site to get devices from
- threshold date: the designated starting date / time for the inclusion of outages

## Available Scripts

In the project directory, you can run:

### `npm run build`

Builds the javascript and puts it in the `dist` folder

### `npm run clean`

Removes the `dist` folder<br>

### `npm run test`

Runs the unit tests once

### `npm run test:watch`

Runs the unit tests and re-runs the units tests on any relevant file change

## Extension

The main function of this package is built on the foundation of interfaces.

If there is a different place to get site data, just create a new `siteService` that conforms to the `SiteDataSource` interface. The same for the `outageService` and the `siteOutageService`.
